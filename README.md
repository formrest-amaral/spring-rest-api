# REST API
  César Freire @ 2021-12-20


## deploy

activate profile linux  
`$ export spring_profiles_active=<profile>`

activate profile windows  
`$ set spring_profiles_active=<profile>`

local run  
`$ ./mvnw spring-boot:run`

local run with profile  
`$ ./mvnw spring-boot:run -Dspring-boot.run.profiles=development`

run final solution  
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`

---

## build

clean target  
`$ ./mvnw clean`

target build  
`$ ./mvnw package`

Docker build  

```sh
$ git clone git@gitlab.com:form-cfreire/spring-rest-api.git
$ cd spring-rest-api
$ docker build -t spring-rest-api:latest .
```


## Tests

`$ ./mvnw test`


## CI/CD

`$ sudo apk add gitlab-runner`

__production server__
```
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "gbygfJYrFboDJWkemykY" \
  --executor "shell" \
  --description "docker-lab-node2" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
```

__development server__
```
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "gbygfJYrFboDJWkemykY" \
  --executor "shell" \
  --description "docker-lab-node2" \
  --tag-list "development_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
```

__test server__
```
sudo gitlab-runner register \
--non-interactive \
--url "https://gitlab.com/" \
--registration-token "gbygfJYrFboDJWkemykY" \
--executor "docker" \
--description "docker-lab-node2" \
--tag-list "develop_server_docker" \
--docker-image "maven:3-openjdk-17-slim" \
--run-untagged="false" \
--locked="false" \
--access-level="not_protected"
```

`$ gitlab-runner run &`

