package pt.rumos.rest_api;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import pt.rumos.rest_api.simulator.RestApiController;

@SpringBootTest
public class RestControllerTests {
    
    @Autowired
    RestApiController controller;

    @Test
    void testIsPresent(){
        assertNotNull(controller);
    }
}
